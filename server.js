const server = require('http').createServer();
const options = {
    cors:true,
};

const PORT = process.env.PORT || 5000;

const io = require('socket.io')(server, options);
server.listen(PORT);

io.on('connection', socket => {
    const id = socket.handshake.query.id;
    socket.join(id);

    socket.on('send-message', ({ recepients, text })=> {
        recepients.forEach(recepient => {
            const newRecepients = recepients.filter(r => r!== recepient);
            newRecepients.push(id);
            socket.broadcast.to(recepient).emit('receive-message', {
                recepients: newRecepients, message: text, sender: id
            });
        });
    })
});